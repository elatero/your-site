import React from 'react'
import { Route, Switch } from 'react-router'
import MainPage from './MainPage';
import SecondPage from './SecondPage';

const Router = (props) => {
  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/" component={MainPage}/>
        <Route to="/second" exact component={SecondPage}/>
      </Switch>
    </React.Fragment>
  )
}

export default Router