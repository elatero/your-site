import { ISCHECKED, NOCHECKED, ISAUTH, NOAUTH } from './actionTypes'

export function onChecked() {
  if(!localStorage.getItem("isChecked")){
    localStorage.setItem("isChecked", true)
    return {
      type: ISCHECKED
    }
  } else {
    localStorage.clear()
    return {
      type: NOCHECKED
    }
  } 
}

export function isChecked() {
  if (localStorage.getItem("isChecked") === "true") {
    return {
      type: ISCHECKED
    }
  } else {
    return {
      type: NOCHECKED
    }
  }
}

function getData() {
  const getData = () => new Promise(resolve => {
		setTimeout(() => resolve({jwt: "86fasfgfsogHGad"}), 3000)
  })

  return getData()
}

export function handleSubmit(e) {
  e.preventDefault()
  localStorage.setItem("jwt", "86fasfgfsogHGad")
  return async dispatch => {
    const response = await getData().then(data => data.jwt)

    const ls = localStorage.getItem("jwt")

    if( response === ls){
      console.log(true)
      dispatch(isAuth())
    } else {
      console.log(false)
      dispatch(noAuth())
    }
  }  
}

function isAuth() {
  return {
    type: ISAUTH
  }
}

function noAuth() {
  return {
    type: NOAUTH
  }
}