import { ISCHECKED, NOCHECKED, ISAUTH, NOAUTH } from './actionTypes'

const initialState = {
  isChecked: false,
  autorization: false
}

const mainPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case ISCHECKED:
      return {
        ...state, isChecked: true
      }
    case NOCHECKED:
      return {
        ...state, isChecked: false
      }
    case ISAUTH:
      return {
        ...state, autorization: true
      }
    case NOAUTH:
      return {
        ...state, autorization: false
      }
    default:
      return state
  }
}

export default mainPageReducer