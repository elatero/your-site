import React from 'react'
import { Container, Grid, Avatar,
					FormControl, TextField,
					FormControlLabel, Checkbox,
					Button, Link
				} from '@material-ui/core';
import Icon from './img/icon.svg';
import styles from './MainPage.module.sass';
import { connect } from 'react-redux';
import { onChecked, isChecked, handleSubmit } from './action';
import { withRouter } from "react-router-dom"

const MainPage = (props) => {

	React.useEffect(
		() => {

		props.isChecked()
	
				if (props.auth) {
					props.history.push( '/second' )
				}
	 
	}, [props, props.auth])	

	return (
		<Container maxWidth="lg">
			{console.log(props.auth)}
			<Grid
				container 
				direction="column" 
				justify="center" 
				alignItems="center"
				wrap="nowrap"
				className={styles.container}
			>
				<Avatar alt="logo" src={Icon} className={styles.avatar} />
				<h1 className={styles.title}>Вход в аккаунт</h1>
				<FormControl fullWidth={true}>
					<TextField
						id="outlined-email-input"
						label="Почта"
						fullWidth={true}
						className={styles.email}
						type="email"
						name="email"
						autoComplete="email"
						margin="normal"
        		variant="outlined"
						required
					/>
					<TextField
						id="outlined-password-input"
						label="Пароль"
						fullWidth={true}
						className={styles.password}
						type="password"
						autoComplete="current-password"
						margin="normal"
        		variant="outlined"
						required
					/>
					<FormControlLabel
						control={
							<Checkbox
								checked={props.checked}
								onChange={props.onChecked}
								value={props.checked}
								color="primary"
							/>
						}
						label="Запомнить меня"
					/>
					<Button 
						variant="contained" 
						color="primary" 
						className={styles.button}
						fullWidth={true}
						type="submit"
						onClick={(e)=>{props.handleSubmit(e)}}
					>
					 Войти в аккаунт
					</Button>					
				</FormControl>
				<div className={styles.boxLink}>
					<Link
						component="button"
						variant="body2"
						onClick={() => {
							alert("I'm a button.");
						}}
						className={styles.link}
					>
						Забыли пароль?
					</Link>
					<Link
						component="button"
						variant="body2"
						onClick={() => {
							alert("I'm a button.");
						}}
						className={styles.link}
					>
						Ещё нет аккаунта? Регистрация
					</Link>
				</div>
				<p className={styles.copyright}>Copyright© Ваш сайт 2019.</p>		
			</Grid>	
		</Container>
	)
}

function mapStateToProps(state) {
	return {
		checked: state.mainPage.isChecked,
		auth: state.mainPage.autorization
	}
}

function mapDispatchToProps(dispatch) {
	return {
		onChecked: () => dispatch(onChecked()),
		isChecked: () => dispatch(isChecked()),
		handleSubmit: (e) => dispatch(handleSubmit(e))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MainPage))