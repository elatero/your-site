import React from 'react';
import styles from './SecondPage.module.sass';
import Icon from './img/icon.svg';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { onChecked } from './../MainPage/action';

const SecondPage = (props) => {
  return (
    <div className={styles.SecondPage}>
      <div className={styles.container}>
        <div className={styles.box}>
          <img src={Icon} alt="logo" className={styles.logo}/>
          <h1 className={styles.title}>Вход в аккаунт</h1>
          <div className={styles.form}>
            <div className={styles.boxInput}>
              <input type="email" placeholder="Почта*" className={styles.email}/>
            </div>
            <div className={styles.boxInput}>
              <input type="password" placeholder="Пароль*" className={styles.password}/>
            </div>            
            <div className={styles.checkbox}>
              <label htmlFor="remember" className={styles.checkboxLabel}>
                <input   
                  type="checkbox" 
                  id="remember" 
                  className={styles.checkboxInput} 
                  checked={props.checked}
                  onChange={props.onChecked}
                  value={props.checked}  
                />         
                <i className={styles.checkboxIcon}></i><span>Запомнить меня</span>
                {console.log(props)}
              </label>
            </div>
            <button className={styles.btn}>
              Войти в аккаунт
            </button>
            <div className={styles.boxLink}>
              <Link to="#" className={styles.forget}>Забыли пароль?</Link>
              <Link to="#" className={styles.registration}>Ещё нет аккаунта? Регистрация</Link>
            </div>
          </div>
          <p className={styles.copyright}>Copyright© Ваш сайт 2019.</p>
        </div>
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  return {
    checked: state.mainPage.isChecked
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onChecked: () => dispatch(onChecked())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SecondPage)