import { combineReducers } from 'redux'
import mainPage from '../Features/MainPage/reducer'

export default combineReducers({
  mainPage
})