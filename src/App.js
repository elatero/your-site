import React from 'react';
import Router from './Features/Router'

function App() {
  return (
    <React.Fragment>
      <Router />
    </React.Fragment>
  )
}

export default App
